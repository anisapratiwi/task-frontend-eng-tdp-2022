import "assets/css/style.css";
import "assets/css/custom.css";

import { Route, Routes, Navigate } from "react-router-dom";

import Home from "./home/MainHome";
import Shop from "./shop/MainShop";
import Product from "./shop/MainDetailProduct";
import Login from "./login/MainLogin";
import Profile from "./profile/MyProfile";

const App = () => {
  const RequireAuth = ({ children }) => {
    const isLoggedIn = window.localStorage.getItem("isLoggedIn");
    return isLoggedIn ? children : <Navigate to="/login" />;
  };
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/shop" element={<Shop />} />
        <Route path="/shop/:productId" element={<Product />} />
        <Route path="/login" element={<Login />} />
        <Route
          path="/profile"
          element={
            <RequireAuth>
              <Profile />
            </RequireAuth>
          }
        />
      </Routes>
    </>
  );
};

export default App;
