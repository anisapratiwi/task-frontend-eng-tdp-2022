import Navbar from "../component/Navbar";
import Footer from "../component/Footer";
import Products from "./component/Products";

const MainShop = () => {
  return (
    <>
      <Navbar />

      <div className="content padding" style={{ maxWidth: "1564px" }}>
        <Products />
      </div>

      <Footer />
    </>
  );
};

export default MainShop;
