import axios from "axios";
import { useState, useEffect } from "react";
import { Link, useSearchParams } from "react-router-dom";
import Loading from "component/Loading";

const Products = () => {
  const [listProducts, setListProducts] = useState([]);
  const [searchParams] = useSearchParams();
  const categoryId = searchParams.get("category");
  const [isLoading, setIsLoading] = useState(false);
  const [categoryName, setCategoryName] = useState("");

  const convertMoney = (money) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(money);
  };

  useEffect(() => {
    setIsLoading(true);
    let urlLink = categoryId
      ? `https://api-tdp-2022.vercel.app/api/products?category=${categoryId}`
      : "https://api-tdp-2022.vercel.app/api/products";

    axios
      .get(urlLink)
      .then(function (response) {
        const { data } = response.data;
        setListProducts(data.productList);
        setIsLoading(false);
        if (categoryId) {
          setCategoryName(`Product Category ${data.categoryDetail.name}`);
        } else {
          setCategoryName("All Product");
        }
      })
      .catch(function (error) {
        console.log("eror", error);
      });
  }, [categoryId]);

  return isLoading ? (
    <div style={{ marginTop: "50px" }}>
      <Loading />
    </div>
  ) : (
    <>
      <div className="container padding-32" id="about">
        <h3 className="border-bottom border-light-grey padding-16">
          {categoryName}
        </h3>
      </div>
      <div
        className="row-padding rm-before-after"
        style={{
          display: "flex",
          flexWrap: "wrap",
          justifyContent: "center",
        }}
      >
        {listProducts?.map((data, key) => {
          return (
            <div className="product-card" key={key}>
              {data.discount > 0 ? <div className="badge">Discount</div> : ""}

              <div className="product-tumb">
                <img src={data?.image} alt="product1" />
              </div>
              <div className="product-details">
                <span className="product-catagory">{data?.category}</span>
                <h4>
                  <a href={`/shop/1`}>{data?.title}</a>
                </h4>
                <p>{data.description}</p>
                <div className="product-bottom-details">
                  <div className="product-price">
                    {convertMoney(data?.price)}
                  </div>
                  <div className="product-links">
                    <Link to={`/shop/${data?.id}`}>
                      View Detail<i className="fa fa-heart"></i>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default Products;
