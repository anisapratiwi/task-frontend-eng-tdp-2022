import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Navbar from "../component/Navbar";
import Loading from "component/Loading";
import Footer from "component/Footer";

const MainDetailProduct = () => {
  let params = useParams();
  const [dataProduct, setDataProduct] = useState([]);
  const [quantity, setQuantity] = useState(0);
  const [subTotal, setSubTotal] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  let productId = params?.productId;
  const minQuantity = 1;

  const convertMoney = (money) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(money);
  };

  useEffect(() => {
    setIsLoading(true);
    axios
      .get(`https://api-tdp-2022.vercel.app/api/products/${productId}`)
      .then(function (response) {
        const { data } = response;

        setDataProduct(data.data);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, [productId]);

  useEffect(() => {
    setSubTotal(
      quantity * dataProduct?.price -
        (quantity * dataProduct?.price * dataProduct?.discount) / 100
    );

    setTotalPrice(quantity * dataProduct?.price);
  }, [quantity, dataProduct?.price, dataProduct?.discount]);

  const handleOnChangeQuantity = (event) => {
    let quantityValue = event.target.value;

    if (dataProduct.stock === 0) {
      quantityValue = 0;
    }
    setQuantity(quantityValue);
  };

  const handleAddToCart = () => {
    const isLoggedIn = localStorage.getItem("isLoggedIn");

    if (isLoggedIn) {
      alert("Product added to cart successfully");
      window.location.replace("/");
    } else {
      window.location.replace("/login");
    }
  };

  // console.log(dataProduct);

  return (
    <>
      <Navbar />

      <div className="content padding" style={{ maxWidth: "1564px" }}>
        <div className="container padding-32" id="about">
          <h3 className="border-bottom border-light-grey padding-16">
            Product Information
          </h3>

          {isLoading ? (
            <Loading />
          ) : (
            <div
              className="row-padding card card-shadow padding-large"
              style={{ marginTop: "50px" }}
            >
              <div className="col l3 m6 margin-bottom">
                <div className="product-tumb">
                  <img src={dataProduct.image} alt="Product 1" />
                </div>
              </div>
              <div className="col m6 margin-bottom">
                <h3>{dataProduct.title}</h3>
                <div style={{ marginBottom: "32px" }}>
                  <span>
                    Category : <strong>{dataProduct.category}</strong>
                  </span>
                  <span style={{ marginLeft: "30px" }}>
                    Review : <strong>{dataProduct.rate}</strong>
                  </span>
                  <span style={{ marginLeft: "30px" }}>
                    Stock : <strong>{dataProduct.stock}</strong>
                  </span>
                  <span style={{ marginLeft: "30px" }}>
                    Discount : <strong>{dataProduct.discount} %</strong>
                  </span>
                </div>
                <div
                  style={{
                    fontSize: "2rem",
                    lineHeight: "34px",
                    fontWeight: "800",
                    marginBottom: "32px",
                  }}
                >
                  {convertMoney(dataProduct.price)}
                </div>
                <div style={{ marginBottom: "32px" }}>
                  {dataProduct.description}
                </div>
                <div style={{ marginBottom: "32px" }}>
                  <div>
                    <strong>Quantity : </strong>
                  </div>
                  <input
                    type="number"
                    className="input section border"
                    name="total"
                    id="total"
                    placeholder="Quantity"
                    onChange={handleOnChangeQuantity}
                    value={quantity}
                    max={dataProduct.stock}
                    min={minQuantity}
                  />
                </div>
                <div
                  style={{
                    marginBottom: "32px",
                    fontSize: "2rem",
                    fontWeight: 800,
                  }}
                >
                  Sub Total : {convertMoney(subTotal)}
                  <span
                    style={{
                      marginLeft: "30px",
                      fontSize: "18px",
                      textDecoration: "line-through",
                    }}
                  >
                    <strong>{convertMoney(totalPrice)}</strong>
                  </span>
                </div>
                {dataProduct.stock === 0 ? (
                  <button className="button light-grey block" disabled={true}>
                    Empty Stock
                  </button>
                ) : quantity === 0 ? (
                  <button
                    className="button light-grey block"
                    onClick={handleAddToCart}
                    disabled={true}
                  >
                    Add to cart
                  </button>
                ) : (
                  <button
                    className="button light-grey block"
                    onClick={handleAddToCart}
                  >
                    Add to cart
                  </button>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default MainDetailProduct;
