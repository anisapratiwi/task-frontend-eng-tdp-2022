import { useState } from "react";
import Navbar from "../component/Navbar";
import axios from "axios";
import Footer from "component/Footer";

const MainLogin = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handleUsername = (event) => {
    setUsername(event.target.value);
  };

  const handlePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setIsLoading(true);

    axios
      .post("https://api-tdp-2022.vercel.app/api/login", {
        username,
        password,
      })
      .then(function (response) {
        const { data } = response;
        localStorage.setItem("token", data.data.access_token);
        localStorage.setItem("user_info", username);
        localStorage.setItem("isLoggedIn", true);
        setIsLoading(false);

        window.location.replace("/");
      })
      .catch(function (error) {
        alert(error?.response?.data?.message);
        setIsLoading(false);
        // console.log("eror", error);
      });
  };

  return (
    <>
      <Navbar />
      <div className="content padding" style={{ maxWidth: "1564px" }}>
        <div className="container padding-32" id="contact">
          <h3 className="border-bottom border-light-grey padding-16">Login</h3>
          <p>Lets get in touch and talk about your next project.</p>
          <form onSubmit={handleSubmit}>
            <input
              className="input border"
              type="text"
              placeholder="Username"
              required
              name="username"
              onChange={handleUsername}
            />
            <input
              className="input section border"
              type="password"
              placeholder="Password"
              required
              name="Password"
              onChange={handlePassword}
            />
            <button
              className="button black section"
              type="submit"
              disabled={isLoading}
            >
              <i className="fa fa-paper-plane" />
              {isLoading ? "Loading" : "Login"}
            </button>
          </form>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default MainLogin;
