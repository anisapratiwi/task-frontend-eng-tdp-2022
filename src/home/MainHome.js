import Navbar from "../component/Navbar";
import Header from "../component/Header";
import ProductCategory from "../component/ProductCategory";
import Maps from "../component/Maps";
import Footer from "../component/Footer";

const MainHome = () => {
  return (
    <>
      <Navbar />
      <Header />
      <ProductCategory />
      <Maps />
      <Footer />
    </>
  );
};

export default MainHome;
