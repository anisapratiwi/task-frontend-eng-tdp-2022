const ErrorMessage = ({ message }) => {
  return <div style={{ color: "#EF144A" }}>{message}</div>;
};

export default ErrorMessage;
