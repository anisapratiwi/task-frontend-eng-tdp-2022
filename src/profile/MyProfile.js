import Navbar from "component/Navbar";
import React, { useEffect, useState } from "react";
import axios from "axios";
import ErrorMessage from "./component/ErrorMessage";
import Footer from "component/Footer";

const MyProfile = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [retypePassword, setRetypePassword] = useState("");
  const [isLoadingBtn, setIsLoadingBtn] = useState(false);
  const [messageName, setMessageName] = useState("");
  const [messageEmail, setMessageEmail] = useState("");
  const [messagePhoneNumber, setMessagePhoneNumber] = useState("");
  const [messagePassword, setMessagePassword] = useState("");
  const [messageRetypePassword, setMessageRetypePassword] = useState("");
  const userName = localStorage.getItem("user_info");
  let statusValid = true;

  useEffect(() => {
    axios
      .get(`https://api-tdp-2022.vercel.app/api/profile/${userName}`)
      .then(function (response) {
        const { data } = response?.data;
        setName(data?.name);
        setEmail(data?.email);
        setPhoneNumber(data?.phoneNumber);
      })
      .catch(function (e) {});
  }, [userName]);

  //   Validation
  const validationName = () => {
    if (!name) {
      setMessageName("Name is required");
      statusValid = false;
    } else {
      setMessageName("");
    }
  };

  const validationEmail = () => {
    if (!email) {
      setMessageEmail("Email is required");
      statusValid = false;
    } else {
      let cond = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

      if (!email.match(cond)) {
        statusValid = false;
        setMessageEmail("Email invalid");
      } else {
        setMessageEmail("");
      }
    }
  };

  const validationPhone = () => {
    if (!phoneNumber) {
      setMessagePhoneNumber("Phone number is required");
      statusValid = false;
    } else {
      if (!phoneNumber.match(/^[0-9]{12}$/)) {
        statusValid = false;
        setMessagePhoneNumber("Phone number invalid");
      } else {
        setMessagePhoneNumber("");
      }
    }
  };

  const validationPassword = () => {
    if (!password) {
      setMessagePassword("Password is required");
      statusValid = false;
    } else {
      setMessagePassword("");
    }
  };

  const validationRetypePassword = () => {
    if (!retypePassword) {
      setMessageRetypePassword("Retype Password is required");
      statusValid = false;
    } else if (password !== retypePassword) {
      setMessageRetypePassword("Retype Password invalid");
      statusValid = false;
    } else {
      setMessageRetypePassword("");
    }
  };
  // ------

  const handleSubmit = (event) => {
    event.preventDefault();
    validationName();
    validationEmail();
    validationPhone();
    validationPassword();
    validationRetypePassword();

    if (statusValid) {
      setIsLoadingBtn(true);
      axios
        .put(`https://api-tdp-2022.vercel.app/api/profile/${userName}`, {
          name,
          email,
          phoneNumber,
          password,
        })
        .then(function (response) {
          const { data } = response;
          alert(data?.message);
          window.location.reload();
          setIsLoadingBtn(false);
        })
        .catch(function (event) {
          alert(event?.response?.data?.message);
          setIsLoadingBtn(false);
        });
    }
  };

  return (
    <>
      <Navbar />
      <div className="content padding" style={{ maxWidth: "1564px" }}>
        <div className="container padding-32" id="contact">
          <h3 className="border-bottom border-light-grey padding-16">
            My Profile
          </h3>
          <p>Lets get in touch and talk about your next project.</p>
          <form onSubmit={handleSubmit}>
            <input
              className="input section border"
              type="text"
              placeholder="Name"
              name="name"
              value={name}
              onChange={(event) => setName(event?.target?.value)}
            />
            <ErrorMessage message={messageName} />
            <input
              className="input section border"
              type="text"
              placeholder="Email"
              name="email"
              value={email}
              onChange={(event) => setEmail(event?.target?.value)}
            />
            <ErrorMessage message={messageEmail} />

            <input
              className="input section border"
              type="text"
              placeholder="Phone Number"
              name="phoneNumber"
              maxLength={12}
              value={phoneNumber}
              onChange={(event) => setPhoneNumber(event?.target?.value)}
            />
            <ErrorMessage message={messagePhoneNumber} />
            <input
              className="input section border"
              type="password"
              placeholder="Password"
              name="password"
              value={password}
              onChange={(event) => setPassword(event?.target?.value)}
            />
            <ErrorMessage message={messagePassword} />
            <input
              className="input section border"
              type="password"
              placeholder="Retype Password"
              name="retypePassword"
              value={retypePassword}
              onChange={(event) => setRetypePassword(event?.target?.value)}
            />
            <ErrorMessage message={messageRetypePassword} />

            <button className="button black section" type="submit">
              <i className="fa fa-paper-plane" />{" "}
              {isLoadingBtn ? "Loading" : "Login"}
            </button>
          </form>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default MyProfile;
