import { useState, useEffect } from "react";
import axios from "axios";

const ProductCategory = () => {
  const [listCategory, setlistCategory] = useState([]);

  useEffect(() => {
    axios
      .get(`https://api-tdp-2022.vercel.app/api/categories`)
      .then(function (response) {
        const { data } = response;

        setlistCategory(data.data);
      })
      .catch(function (error) {
        console.log("error", error);
      });
  }, []);

  // console.log(listCategory);

  return (
    <div>
      <div className="container padding-32" id="projects">
        <h3 className="border-bottom border-light-grey padding-16">
          Products Category
        </h3>
      </div>
      <div className="row-padding">
        {listCategory.map((data, key) => {
          return (
            <div className="col l3 m6 margin-bottom" key={key}>
              <a href={`/shop?category=${data?.id}`}>
                <div
                  className="display-container"
                  style={{
                    boxShadow: "0 2px 7px #dfdfdf",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    height: "300px",
                    padding: "80px",
                  }}
                >
                  <div className="display-topleft black padding">
                    {data.name}
                  </div>
                  <img
                    src={data.image}
                    alt={data.name}
                    style={{ maxWidth: "100%", minHeight: "100%" }}
                  />
                </div>
              </a>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default ProductCategory;
