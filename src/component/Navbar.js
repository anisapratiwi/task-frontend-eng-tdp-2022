const Navbar = () => {
  const isLoggedIn = localStorage.getItem("isLoggedIn");

  const handleLogout = () => {
    localStorage.clear();
    window.location.reload();
  };
  return (
    <div className="top">
      <div className="bar white wide padding card">
        <a href="/" className="bar-item button">
          <b>EDTS</b> TDP Batch #2
        </a>
        <div className="right hide-small">
          <a href="/" className="bar-item button">
            Home
          </a>
          <a href="/shop" className="bar-item button">
            Shop
          </a>
          {isLoggedIn ? (
            <a href="/profile" className="bar-item button">
              My Profile
            </a>
          ) : (
            ""
          )}
          {isLoggedIn ? (
            <a href="/#" className="bar-item button" onClick={handleLogout}>
              Logout{" "}
            </a>
          ) : (
            <a href="/login" className="bar-item button">
              Login
            </a>
          )}
        </div>
      </div>
    </div>
  );
};

export default Navbar;
