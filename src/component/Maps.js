import map from "assets/img/map.jpg";

const Maps = () => {
  return (
    <div className="content padding" style={{ maxWidth: "1564px" }}>
      <div className="container padding-32">
        <img src={map} className="image" alt="maps" style={{ width: "100%" }} />
      </div>
    </div>
  );
};

export default Maps;
